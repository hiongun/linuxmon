package main

import (
    "os/exec"
    "strings"
    "fmt"
)

func unix_backquote(cmd_str string) []string {
    args := strings.Split(cmd_str, " ")

    cmd := exec.Command(args[0], args[1:]...)
    output, err := cmd.CombinedOutput()

    if err != nil {
        fmt.Printf("-ERROR %s\n", err.Error())
        return make([]string, 0)
    }

    lines := strings.Split(string(output), "\n")
    return lines
}

//COMMAND     PID      USER   FD      TYPE             DEVICE SIZE/OFF     NODE NAME
//init          1      root  cwd   unknown                                      /proc/1/cwd (readlink: Permission denied)
//init          1      root  rtd   unknown                                      /proc/1/root (readlink: Permission denied)
//init          1      root  txt   unknown                                      /proc/1/exe (readlink: Permission denied)
//init          1      root NOFD                                                /proc/1/fd (opendir: Permission denied)
//kthreadd      2      root  cwd   unknown                                      /proc/2/cwd (readlink: Permission denied)
//kthreadd      2      root  rtd   unknown                                      /proc/2/root (readlink: Permission denied)
//kthreadd      2      root  txt   unknown                                      /proc/2/exe (readlink: Permission denied)
//kthreadd      2      root NOFD                                                /proc/2/fd (opendir: Permission denied)
//ksoftirqd     3      root  cwd   unknown                                      /proc/3/cwd (readlink: Permission denied)


func lsof_decode() map[string]float64 {
    result := map[string]float64{}

    lines := unix_backquote("lsof")

    users := map[string]int{}
    cmds := map[string]int{}

    result["n_lines"] = 0

    for _, line := range(lines) {
        row := strings.Split(line, " ")
        result["n_lines"] += 1
        if len(row) > 2 {
            cmds[row[0]] = 1
            users[row[2]] = 1
        }
    }
    result["n_users"] = float64(len(users))
    result["n_cmds"] = float64(len(cmds))

    return result
}

/*
//
//top - 20:27:57 up 235 days, 10:40,  2 users,  load average: 0.00, 0.01, 0.05
//Tasks: 195 total,   1 running, 194 sleeping,   0 stopped,   0 zombie
//Cpu(s):  0.1%us,  0.7%sy,  0.0%ni, 99.1%id,  0.0%wa,  0.0%hi,  0.0%si,  0.0%st
//Mem:    942136k total,   885720k used,    56416k free,   157488k buffers
//Swap: 67108860k total,   191684k used, 66917176k free,   438804k cached

func top_decode() map[string]float64 {
    result := map[string]float64{}
    lines := unix_backquote("top -b -n 1")

}



//UID        PID  PPID  C STIME TTY          TIME CMD
//root         1     0  0 Apr10 ?        00:00:26 /sbin/init
//root         2     0  0 Apr10 ?        00:00:00 [kthreadd]
//root         3     2  0 Apr10 ?        00:01:37 [ksoftirqd/0]
//root         5     2  0 Apr10 ?        00:00:00 [kworker/0:0H]
//root         7     2  0 Apr10 ?        00:00:01 [migration/0]
//root         8     2  0 Apr10 ?        00:00:00 [rcu_bh]

func ps_decode() map[string]float64 {

}


//Linux 3.10.43-11.el6.centos.alt.x86_64 (hadoop01)       12/01/2015      _x86_64_        (8 CPU)
//
//avg-cpu:  %user   %nice %system %iowait  %steal   %idle
//           0.16    0.00    0.04    0.00    0.00   99.80
//
//Device:            tps   Blk_read/s   Blk_wrtn/s   Blk_read   Blk_wrtn
//sdb               0.00         0.00         0.00       8394        704
//sda               0.53        55.67        38.72 1132590486  787670156
//sdd               0.00         0.00         0.00       8082        720
//sdc               0.00         0.00         0.00       8010        624

func iostat_decode() map[string]float64 {

}


//ifconfig -a
//br0       Link encap:Ethernet  HWaddr D8:9D:67:29:9F:C7
//          inet addr:192.168.200.201  Bcast:192.168.200.255  Mask:255.255.255.0
//          inet6 addr: fe80::da9d:67ff:fe29:9fc7/64 Scope:Link
//          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
//          RX packets:19225070 errors:0 dropped:0 overruns:0 frame:0
//          TX packets:8908142 errors:0 dropped:0 overruns:0 carrier:0
//          collisions:0 txqueuelen:0
//          RX bytes:9670338709 (9.0 GiB)  TX bytes:8081767492 (7.5 GiB)
//
//eth0      Link encap:Ethernet  HWaddr D8:9D:67:29:9F:C4
//          inet addr:192.168.0.201  Bcast:192.168.0.255  Mask:255.255.255.0
//          inet6 addr: fe80::da9d:67ff:fe29:9fc4/64 Scope:Link
//          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
//          RX packets:265778284 errors:8 dropped:1190 overruns:0 frame:8
//          TX packets:5558632 errors:0 dropped:0 overruns:0 carrier:0
//          collisions:0 txqueuelen:1000
//          RX bytes:31797995113 (29.6 GiB)  TX bytes:4988384054 (4.6 GiB)
//          Interrupt:32

func ifconfig_decode() map[string]float64 {

}


//df -ak
//df: `/root/.gvfs': Permission denied
//Filesystem     1K-blocks      Used Available Use% Mounted on
///dev/sda2      103081248  54845648  42976336  57% /
//proc                   0         0         0    - /proc
//sysfs                  0         0         0    - /sys
//devpts                 0         0         0    - /dev/pts
//tmpfs             471068       228    470840   1% /dev/shm
///dev/sda5      791799392 382882004 368673216  51% /DATA
///dev/sdb1      961271120 342541872 569876464  38% /DATA1
///dev/sdc1      961271120 368276200 544142136  41% /DATA2
///dev/sdd1      961271120 329176552 583241784  37% /DATA3
///dev/sda1         194241     95035     84870  53% /boot
//none                   0         0         0    - /proc/sys/fs/binfmt_misc
//xenstore          471068        40    471028   1% /var/lib/xenstored

func df_decode() map[string]float64 {

}

*/

func main() {
    result := lsof_decode()
    for k, v := range(result) {
        fmt.Printf("%s: %.1f\n", k, v)
    }

/*
    result = top_decode()
    for k, v := range(result) {
        fmt.Printf("%s: %.1f\n", k, v)
    }
*/
}

