#!/bin/env perl

sub ps_decode($)
{
    my ($result) = @_;

    my @lines = `ps -ef 2>/dev/null`;

    # UID        PID  PPID  C STIME TTY          TIME CMD
    # root         1     0  0 Apr10 ?        00:00:26 /sbin/init
    # root         2     0  0 Apr10 ?        00:00:00 [kthreadd]
    # root         3     2  0 Apr10 ?        00:01:37 [ksoftirqd/0]
    # root         5     2  0 Apr10 ?        00:00:00 [kworker/0:0H]
    # root         7     2  0 Apr10 ?        00:00:01 [migration/0]
    # root         8     2  0 Apr10 ?        00:00:00 [rcu_bh]

    $result->{"ps.n_lines"} = scalar(@lines);
    $result->{"ps.n_java"} = 0;
    $result->{"ps.n_python"} = 0;
    $result->{"ps.n_perl"} = 0;
    $result->{"ps.n_zombie"} = 0;

    foreach my $line (@lines) {
        $line =~ s/^\s+//g;
        if ($line =~ /java/) { $result->{"ps.n_java"} += 1; }
        if ($line =~ /defunc/) { $result->{"ps.n_zombie"} += 1; }
        if ($line =~ /perl/) { $result->{"ps.n_perl"} += 1; }
        if ($line =~ /python/) { $result->{"ps.n_python"} += 1; }
    }
}

sub vmstat_decode($)
{
    my ($result) = @_;

    my @lines = `vmstat 2>/dev/null`;

    # procs -----------memory---------- ---swap-- -----io---- --system-- -----cpu-----
    # r  b   swpd   free   buff  cache   si   so    bi    bo   in   cs us sy id wa st
    # 0  0 191684  51648 160288 437052    0    0     3     2    0    0  0  0 100  0  0

    my @keys = ();
    my @vals = ();
    foreach my $line (@lines) {
        $line =~ s/^\s+//g;
        my @row = split(/\s+/, $line);

        if ($line =~ /^[\s+a-z]+$/) {
            @keys = @row;
            next;
        }

        if ($line =~ /^[\s+0-9]+$/) {
            @vals = @row;
        }
    }

    if (@keys == @vals) {
        for (my $i = 0; $i < @keys; $i++) {
            my $key = $keys[$i];
            $result->{"vmstat.$key"} = $vals[$i];
        }
    }
}

sub iostat_decode($)
{
    my ($result) = @_;

    my @lines = `iostat 2>/dev/null`;

    # $ iostat
    # Linux 2.6.32-100.28.5.el6.x86_64 (dev-db)       07/09/2011
    # avg-cpu:  %user   %nice %system %iowait  %steal   %idle
    #       5.68    0.00    0.52    2.03    0.00   91.76
    # Device:            tps   Blk_read/s   Blk_wrtn/s   Blk_read   Blk_wrtn
    # sda             194.72      1096.66      1598.70 2719068704 3963827344
    # sda1            178.20       773.45      1329.09 1917686794 3295354888
    # sda2             16.51       323.19       269.61  801326686  668472456
    # sdb             371.31       945.97      1073.33 2345452365 2661206408
    # sdb1            371.31       945.95      1073.33 2345396901 2661206408
    # sdc             408.03       207.05       972.42  513364213 2411023092
    # sdc1            408.03       207.03       972.42  513308749 2411023092

    my $device_begin = 0;
    foreach my $line (@lines) {
        my @row = split(/\s+/, $line);

        if ($row[0] eq 'Device:') {
            $device_begin = 1;
            next;
        }

        if ($device_begin && @row >= 6) {
            $result->{"iostat.tps.$row[0]"} = $row[1];
            $result->{"iostat.blk_read/s.$row[0]"} = $row[2];
            $result->{"iostat.blk_wrtn/s.$row[0]"} = $row[3];
        }
    }
}


sub sar_decode($)
{
    my ($result) = @_;
    my @lines = `sar -b 1 1 2>/dev/null`;

    # sar -b 1 1
    # Linux 3.10.43-11.el6.centos.alt.x86_64 (hadoop01)       12/01/2015      _x86_64_        (8 CPU)

    # 09:44:20 PM       tps      rtps      wtps   bread/s   bwrtn/s
    # 09:44:21 PM      0.00      0.00      0.00      0.00      0.00
    # Average:         0.00      0.00      0.00      0.00      0.00

    foreach my $line (@lines) {
        my @row = split(/\s+/, $line);

        if ($row[0] eq "Average:") {
            $result->{"sar.tps"} = $row[1];
            $result->{"sar.rtps"} = $row[2];
            $result->{"sar.wtps"} = $row[3];
            $result->{"sar.bread/s"} = $row[4];
            $result->{"sar.bwrtn/s"} = $row[5];
        }
    }
}


sub netstat_decode($)
{
    my ($result) = @_;

    my @lines = `netstat -an 2> /dev/null`;

    # Proto Recv-Q Send-Q Local Address               Foreign Address             State
    # tcp        0      0 192.168.122.1:53            0.0.0.0:*                   LISTEN
    # tcp        0      0 0.0.0.0:10022               0.0.0.0:*                   LISTEN
    # tcp        0     52 58.181.37.175:10022         1.221.187.110:7147          ESTABLISHED
    # tcp        0      0 58.181.37.175:48620         173.223.227.26:80           ESTABLISHED
    # tcp        0      0 :::58196                    :::*                        LISTEN
    # tcp        0      0 :::58100                    :::*                        LISTEN
    # tcp        0      0 :::59700                    :::*                        LISTEN
    # tcp        0      0 :::59508                    :::*                        LISTEN
    # tcp        0      0 :::59509                    :::*                        LISTEN
    # tcp        0      0 :::58197                    :::*                        LISTEN
    # tcp        0      0 :::58101                    :::*                        LISTEN

    foreach my $line (@lines) {
        my @row = split(/\s+/, $line);

        if ($row[0] eq 'tcp' || $row[0] eq 'udp') {
            my $port = $row[3];
            $port =~ s/^.+://g;
            my $stat = $row[5];

            # print "netstat.$port.$stat\n";

            if ($port =~ /^\d+$/ && $stat =~ /^[A-Z].+$/) {
                $result->{"netstat.$port.$stat"} += 1;
            }
        }
    }
}

sub df_decode($)
{
    my ($result) = @_;

    my @lines = `df -k 2>/dev/null`;

    # Filesystem     1K-blocks      Used Available Use% Mounted on
    # /dev/sda2      103081248  54845676  42976308  57% /
    # tmpfs             471068       228    470840   1% /dev/shm
    # /dev/sda5      791799392 382882004 368673216  51% /DATA
    # /dev/sdb1      961271120 342541872 569876464  38% /DATA1
    # /dev/sdc1      961271120 368276200 544142136  41% /DATA2
    # /dev/sdd1      961271120 329176552 583241784  37% /DATA3
    # /dev/sda1         194241     95035     84870  53% /boot
    # xenstore          471068        40    471028   1% /var/lib/xenstored

    foreach my $line (@lines) {
        if ($line =~ /(\d+)% (\/.+)/) {
            $result->{"df." . $2} = $1;
        }
    }
}

sub top_decode($)
{
    my ($result) = @_;

    my @lines = `top -b -n 1 2>/dev/null`;

    # top - 21:11:04 up 235 days, 11:23,  2 users,  load average: 0.00, 0.01, 0.05
    # Tasks: 203 total,   1 running, 201 sleeping,   0 stopped,   1 zombie
    # Cpu(s):  0.2%us,  0.0%sy,  0.0%ni, 99.8%id,  0.0%wa,  0.0%hi,  0.0%si,  0.0%st
    # Mem:    942136k total,   887336k used,    54800k free,   158484k buffers
    # Swap: 67108860k total,   191684k used, 66917176k free,   436396k cached

    if (@lines >= 5) {
        if ($lines[0] =~ /(\d+) users,/) {
            $result->{"top.n_users"} = $1;
        }
        if ($lines[0] =~ /load average. ([\d\.]+),/) {
            $result->{"top.load_avg_1m"} = $1;
        }
        if ($lines[1] =~ /(\d+) total,/) {
            $result->{"top.proc_total"} = $1;
        }
        if ($lines[1] =~ /(\d+) running,/) {
            $result->{"top.proc_running"} = $1;
        }
        if ($lines[1] =~ /(\d+) sleeping,/) {
            $result->{"top.proc_sleeping"} = $1;
        }
        if ($lines[1] =~ /(\d+) zombie,/) {
            $result->{"top.proc_zombie"} = $1;
        }

        if ($lines[2] =~ /([\d\.]+)\%us,/) {
            $result->{"top.cpu_user"} = $1;
        }
        if ($lines[2] =~ /([\d\.]+)\%sy,/) {
            $result->{"top.cpu_sys"} = $1;
        }
        if ($lines[2] =~ /([\d\.]+)\%wa,/) {
            $result->{"top.cpu_wait"} = $1;
        }
        if ($lines[3] =~ /([\d\.]+). used,/) {
            $result->{"top.mem_used"} = $1;
        }
        if ($lines[3] =~ /([\d\.]+). free,/) {
            $result->{"top.mem_free"} = $1;
        }
        if ($lines[4] =~ /([\d\.]+). used,/) {
            $result->{"top.swap_used"} = $1;
        }
        if ($lines[4] =~ /([\d\.]+). free,/) {
            $result->{"top.swap_free"} = $1;
        }
    }
}

sub lsof_decode($)
{
    my ($result) = @_;

    my @lines = `lsof 2>/dev/null`;

    # COMMAND     PID      USER   FD      TYPE             DEVICE SIZE/OFF     NODE NAME
    # init          1      root  cwd   unknown                                      /proc/1/cwd (readlink: Permission denied)
    # init          1      root  rtd   unknown                                      /proc/1/root (readlink: Permission denied)
    # init          1      root  txt   unknown                                      /proc/1/exe (readlink: Permission denied)
    # init          1      root NOFD                                                /proc/1/fd (opendir: Permission denied)
    # kthreadd      2      root  cwd   unknown                                      /proc/2/cwd (readlink: Permission denied)
    # kthreadd      2      root  rtd   unknown                                      /proc/2/root (readlink: Permission denied)
    # kthreadd      2      root  txt   unknown                                      /proc/2/exe (readlink: Permission denied)
    # kthreadd      2      root NOFD                                                /proc/2/fd (opendir: Permission denied)
    # ksoftirqd     3      root  cwd   unknown                                      /proc/3/cwd (readlink: Permission denied)

    $result->{"lsof.n_lines"} = scalar(@lines);

    my $users = {};
    my $commands = {};

    foreach my $line (@lines) {
        my @row = split(/\s+/, $line);
        $commands->{$row[0]} += 1;
        $users->{$row[0]} += 1;
    }

    $result->{"lsof.n_users"} = scalar(keys %{$users});
    $result->{"lsof.n_commands"} = scalar(keys %{$commands});
}

sub w_decode($)
{
    my ($result) = @_;

    my @lines = `w 2>/dev/null`;

    # $ w
    # 09:35:06 up 21 days, 23:28,  2 users,  load average: 0.00, 0.00, 0.00
    # USER     TTY      FROM          LOGIN@   IDLE   JCPU   PCPU WHAT
    # root     tty1     :0            24Oct11  21days 1:05   1:05 /usr/bin/Xorg :0 -nr -verbose
    # ramesh   pts/0    192.168.1.10  Mon14    0.00s  15.55s 0.26s sshd: localuser [priv]
    # john     pts/0    192.168.1.11  Mon07    0.00s  19.05s 0.20s sshd: localuser [priv]
    # jason    pts/0    192.168.1.12  Mon07    0.00s  21.15s 0.16s sshd: localuser [priv]

    $result->{"w.n_lines"} = scalar(@lines);

    my $users = {};
    my $USER_begin = 0;
    foreach my $line (@lines) {
        my @row = split(/\s+/, $line);
        if ($line =~ /USER/) {
            $USER_begin = 1;
            next;
        }
        if ($USER_begin) {
            $users->{$row[0]} += 1;
        }
    }

    $result->{"w.n_users"} = scalar(keys %{$users});
}

sub ifconfig_decode($)
{
    my ($result) = @_;

    my @lines = `ifconfig -a 2>/dev/null`;

    # br0       Link encap:Ethernet  HWaddr D8:9D:67:29:9F:C7
    #           inet addr:192.168.200.201  Bcast:192.168.200.255  Mask:255.255.255.0
    #           inet6 addr: fe80::da9d:67ff:fe29:9fc7/64 Scope:Link
    #           UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
    #           RX packets:19227782 errors:0 dropped:0 overruns:0 frame:0
    #           TX packets:8908185 errors:0 dropped:0 overruns:0 carrier:0
    #           collisions:0 txqueuelen:0
    #           RX bytes:9670476759 (9.0 GiB)  TX bytes:8081773198 (7.5 GiB)
    # eth0      Link encap:Ethernet  HWaddr D8:9D:67:29:9F:C4
    #           inet addr:192.168.0.201  Bcast:192.168.0.255  Mask:255.255.255.0
    #           inet6 addr: fe80::da9d:67ff:fe29:9fc4/64 Scope:Link
    #           UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
    #           RX packets:265843710 errors:8 dropped:1190 overruns:0 frame:8
    #           TX packets:5558800 errors:0 dropped:0 overruns:0 carrier:0
    #           collisions:0 txqueuelen:1000
    #           RX bytes:31804738507 (29.6 GiB)  TX bytes:4988394806 (4.6 GiB)
    #           Interrupt:32

    my $ifname = '';
    foreach my $line (@lines) {
        if ($line =~ /^(\S+)\s+Link encap/) {
            $ifname = $1;
            next;
        }

        if ($line =~ /^\s+RX packets:(\d+) errors:(\d+) dropped:(\d+) overruns:(\d+)/) {
            $result->{"ifconfig.$ifname.RX_packets"} = $1;
            $result->{"ifconfig.$ifname.RX_errors"} = $2;
            $result->{"ifconfig.$ifname.RX_dropped"} = $3;
            $result->{"ifconfig.$ifname.RX_overruns"} = $4;
        }
        if ($line =~ /^\s+TX packets:(\d+) errors:(\d+) dropped:(\d+) overruns:(\d+)/) {
            $result->{"ifconfig.$ifname.TX_packets"} = $1;
            $result->{"ifconfig.$ifname.TX_errors"} = $2;
            $result->{"ifconfig.$ifname.TX_dropped"} = $3;
            $result->{"ifconfig.$ifname.TX_overruns"} = $4;
        }
        if ($line =~ /^\s+collisions:(\d+)/) {
            $result->{"ifconfig.$ifname.collisions"} = $1;
        }

    }
}



sub check_commands()
{
    my $commands = ["top", "df", "netstat", "sar", "iostat", "vmstat", "ps", "lsof", "w", "ifconfig"];
    foreach my $command (@{$commands}) {
        my $out = `which $command 2>&1`;
        if ($out =~ /no .+ in/) {
            print "-ERROR $command not found.\n";
        }
    }
}

sub data_insert($$$)
{
    my ($time, $key, $val) = @_;

    $key =~ s/\//_/g;
    $key =~ s/\-/_/g;

    if (! -e "./data/$key.db") {
        my $cmd = "sqlite3 ./data/$key.db 'CREATE TABLE DATA(time VARCHAR(16), val NUMBER); CREATE INDEX DATA_1M_IDX on DATA_1M(time)'";
        `$cmd`;
    }

    my $cmd = "sqlite3 ./data/$key.db 'INSERT INTO DATA(time, val) VALUES ($time, $val)'";
    `$cmd`;
}

sub check_linuxmon()
{
    my $result = {};

    my $time = int(time() / 60 * 60);

    top_decode($result);
    df_decode($result);
    netstat_decode($result);
    sar_decode($result);
    iostat_decode($result);
    vmstat_decode($result);
    ps_decode($result);
    lsof_decode($result);
    w_decode($result);
    ifconfig_decode($result);

    foreach my $key (sort {$a cmp $b} keys %{$result}) {
        print "$key: $result->{$key}\n";
        data_insert($time, $key, $result->{$key});
    }
}

sub my_sleep($)
{
    my ($secs) = @_;
    select(undef, undef, undef, $secs);
}


MAIN:
{
    check_commands();

    my $prev_time = int(time() / 60);

    while (1) {
        my_sleep(1);

        my $new_time = int(time() / 60);

        if ($new_time != $prev_time) {
            $prev_time = $new_time;

            check_linuxmon();
        }
    }
}



