#!/bin/env perl

use threads;
use threads::shared;

BEGIN { push @INC, "./modules" }

use LogTailLast;
use LogTailSimple;
use LogTailRecent;
use LogSignature;

use YAMLTiny;

sub load_yaml($)
{
    my ($svc_yml) = @_;

    my $yaml = YAMLTiny->read($svc_yml);

if (0) {
    my $conf = $yaml->[0];
    my $logs = $conf->{logs};
    foreach my $log (@{$logs}) {
        warn "-----------\n";
        foreach my $key (keys %{$log}) {
            warn "log->{$key} = $log->{$key}\n";
        }
    }
    my $ports = $conf->{ports};
    foreach my $port (@{$ports}) {
        warn "-----------\n";
        foreach my $key (keys %{$port}) {
            warn "port->{$key} = $port->{$key}\n";
        }
    }
}

    return $yaml->[0];
}


sub call_last($$$)
{
    my ($path, $name, $handler) = @_;
    my $p = new LogTailLast($path, $name);
    $p->{log_handle} = $handler;
    $p->scan_log();
}

sub call_recent($$$)
{
    my ($path, $name, $handler) = @_;
    my $p = new LogTailRecent($path, $name);
    $p->{log_handle} = $handler;
    $p->scan_log();
}

sub call_simple($$$)
{
    my ($path, $name, $handler) = @_;
    my $p = new LogTailSimple($path, $name);
    $p->{log_handle} = $handler;
    $p->scan_log();
}

my $time = time() % 60;

my $stat :shared = &share( {} );


sub my_handler($$$)
{
    lock($stat);

    my ($line, $fname, $logname) = @_;

    warn "$logname: $fname ----> $line";


    $stat->{$logname}->{total} += 1;
    if ($line =~ /SUCCESS|OK|DONE/i) {
        $stat->{$logname}->{success} += 1;
    }

    if ($line =~ /FAIL|ERROR|CRITICAL|WARN|EXCEPTION|REJECT/i) {
        $stat->{$logname}->{fail} += 1;
    }

    my $sign = LogSignature::line_signature($line);

    $stat->{$logname}->{signs}->{$sign} = 1;
}

sub my_sleep($)
{
    my ($secs) = @_;
    select(undef, undef, undef, $secs);
}

sub data_insert($$$)
{
    my ($time, $key, $val) = @_;

    $key =~ s/\//_/g;
    $key =~ s/\-/_/g;

    if (! -e "./data/$key.db") {
        my $cmd = "sqlite3 ./data/$key.db 'CREATE TABLE DATA(time VARCHAR(16), val NUMBER); CREATE INDEX DATA_IDX on DATA_1M(time)'";
# print "$cmd\n";
        `$cmd`;
    }

    my $cmd = "sqlite3 ./data/$key.db 'INSERT INTO DATA(time, val) VALUES ($time, $val)'";
# print "$cmd\n";
    `$cmd`;
}

MAIN:
{
    my $svc_yaml = load_yaml("./svc.conf");

    # use Config;
    # $Config{useithreads}
    # or die('Recompile Perl with threads to run this program.');
    # print "------------\n";

    foreach my $log (@{$svc_yaml->{logs}}) {
        my $logname = $log->{name};
        if (! defined $stat->{$logname}) {
            $stat->{$logname} = &share({});
            $stat->{$logname}->{total} = 0;
            $stat->{$logname}->{success} = 0;
            $stat->{$logname}->{fail} = 0;
            $stat->{$logname}->{signs} = &share({});
        }
    }

    foreach my $log (@{$svc_yaml->{logs}}) {
        print "log->{rotate}: $log->{rotate}\n";
        if ($log->{rotate} eq 'simple') {
            threads->new(\&call_simple, $log->{path}, $log->{name}, \&my_handler);
        }

        if ($log->{rotate} eq 'last') {
            threads->new(\&call_last, $log->{path}, $log->{name}, \&my_handler);
        }

        if ($log->{rotate} eq 'recent') {
            threads->new(\&call_recent, $log->{path}, $log->{name}, \&my_handler);
        }
    }


    my $prev_time = int(time() / 60);

    while (1) {
        my_sleep(1);

        my $new_time = int(time() / 60);

        if ($new_time != $prev_time) {
            lock($stat);
            $prev_time = $new_time;

            warn "stat ------ $new_time, $prev_time ------------";
            foreach my $log (keys %{$stat}) {
# print "$log\n";
                print "${log}->{total}: $stat->{$log}->{total}\n";
                print "${log}->{success}: $stat->{$log}->{success}\n";
                print "${log}->{fail}: $stat->{$log}->{fail}\n";
                print "${log}->{signs}: " . scalar(keys %{$stat->{$log}->{signs}}) . "\n";

                data_insert($new_time*60, "log.$log.total", $stat->{$log}->{total});
                data_insert($new_time*60, "log.$log.success", $stat->{$log}->{success});
                data_insert($new_time*60, "log.$log.fail", $stat->{$log}->{fail});
                data_insert($new_time*60, "log.$log.signs", scalar(keys %{$stat->{$log}->{signs}}));

                $stat->{$log}->{total} = 0;
                $stat->{$log}->{success} = 0;
                $stat->{$log}->{fail} = 0;
                $stat->{$log}->{signs} = &share({});
            }
        }
    }
}

