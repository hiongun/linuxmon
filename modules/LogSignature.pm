package LogSignature;

sub word_signature($)  # 특정한 단어 내의 패턴
{
    my ($word) = @_;

    my $sign = '';
    for (my $i = 0, $n = length($word); $i < $n; $i++) {
        my $ch = substr($word, $i, 1);

        if (index(":.+_<>\\!?~`{}[]|=_-)(*&&^%%\$#\@,/~)", $ch) >= 0) {
            if (substr($sign, -1) ne '_') { $sign .= '_'; }
            next;
        }

        if (index("0123456789", $ch) >= 0) {
            if (substr($sign, -1) ne '0') { $sign .= '0'; }
            $next;
        }

        next if (substr($sign, -1) ne '_' && substr($sign, -1) ne '0' && length($sign) > 0);

        if ("A" le $ch && $ch le "Z") { $sign .= $ch; }
    }

    return $sign;
}

sub line_signature($)    # 특정 라인의 로그패턴 구하기
{
    my ($line) = @_;

    my $sign = '';
    $line =~ s/\s+$//g; # strip last
    $line =~ s/^\s+//g; # strip first

    my @items = split(/\s+/, $line);

    foreach $item (@items) {
        $sign .= word_signature($item);
    }

    return $sign;
}

1;
