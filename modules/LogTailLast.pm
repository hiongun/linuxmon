package LogTailLast;

use strict qw(vars refs subs);
use Carp;

my $DEBUG = 0;
sub ASSERT($) { Carp::confess("ASSERT fail\n") if (! $_[0]); }


sub default_log_handle($$)
{
    my ($line, $fname) = @_;
    print "default_log_handle: [$fname] [$line]\n";
}

sub my_sleep($)
{
    my ($secs) = @_;

    select(undef, undef, undef, $secs);
}


sub LogTailLast::new($$$)
{
    my ($classname, $log_dir, $log_name) = @_;

    my $this = {};
       $this->{log_dir} = $log_dir;
       $this->{log_name} = $log_name;

       $this->{last_YYYYMMDD} = "";
       $this->{last_HH} = "";
       $this->{last_mod_1} = 0;
       $this->{last_mod_2} = 0;
       $this->{log_handle} = \&default_log_handle;

   bless($this, $classname);
}

sub LogTailLast::SELECT_LAST($)
{
    my ($this) = @_;

    if (defined $this->{SELECT_LAST_INIT} && $this->{SELECT_LAST_INIT} =~ /^(\d{8})\/(\d{2})$/) {
        my ($last_dir, $last_file) = ($1, $2);
        
        $this->{last_YYYYMMDD} = $last_dir;
        $this->{last_HH} = $last_file;
        $this->{last_mod_1} = (stat($this->{log_dir}))[9];
        $this->{last_mod_2} = (stat("$this->{log_dir}/$last_dir"))[9];

        warn "SELECT_LAST() returns: $this->{log_dir}/$last_dir/$last_file";

        delete $this->{SELECT_LAST_INIT};

        return "$this->{log_dir}/$last_dir/$last_file";
    }

    # warn "SELECT_LAST() called";

    my $mod_1 = (stat($this->{log_dir}))[9];
    my $mod_2 = (stat("$this->{log_dir}/$this->{last_YYYYMMDD}"))[9];
    if ($this->{last_mod_1} < $mod_1 || $this->{last_mod_2} < $mod_2) {
        # do nothing
    } else {
        # warn "SELECT_LAST() returns: $this->{log_dir}/$this->{last_YYYYMMDD}/$this->{last_HH}";
        return "$this->{log_dir}/$this->{last_YYYYMMDD}/$this->{last_HH}";
    }


    my $last_dir = "";
    my $last_file = "";

    my @dirs = (); local(*D);
    if (opendir(D, $this->{log_dir})) {
        @dirs = readdir(D);
        closedir(D);
    }

    if (@dirs == 0) { return undef; }

    foreach my $dir (@dirs) {
        next if ($dir !~ /^\d{8}$/);

        if ($dir gt $last_dir) {
            $last_dir = $dir;
        }
    }

    my @files = ();
    if (opendir(D, "$this->{log_dir}/$last_dir")) {
        @files = readdir(D);
        closedir(D);
    }

    if (@dirs == 0) { return undef; }

    foreach my $file (@files) {
        next if ($file !~ /^\d{2}$/);

        if ($file gt $last_file) {
            $last_file = $file;
        }
    }

    if ($last_dir eq '' || $last_file eq '') {
        return undef;
    }

    $this->{last_YYYYMMDD} = $last_dir;
    $this->{last_HH} = $last_file;
    $this->{last_mod_1} = (stat($this->{log_dir}))[9];
    $this->{last_mod_2} = (stat("$this->{log_dir}/$last_dir"))[9];

    warn "SELECT_LAST() returns: $this->{log_dir}/$last_dir/$last_file";
    return "$this->{log_dir}/$last_dir/$last_file";
}

sub FILE_SKIP($$)
{
    my ($fp, $fsize) = @_;

  # my $n_sum = 0;
  # while ($n_sum < $fsize) {
  #     my $line = <$fp>;
  #     if (! defined $line) {
  #         last;
  #     }
  #     $n_sum += length($line);
  # }

    seek($fp, $fsize, 0) or warn "$!";
}

sub LogTailLast::scan_log($)
{
    my ($this) = @_;

    my $fname = "";
    local(*F);

    while (1) {
        # warn "call SELECT_LAST()";
        $fname = $this->SELECT_LAST();
        if (! $fname) {
            warn "this->SELECT_LAST() failed: $!";
            my_sleep(1);
            next;
        }

        # warn "selected: $fname";

        my $fsize = -s $fname;
        if (! open(F, $fname)) {
            # warn "open(F, $fname) failed: $!";
            my_sleep(1);
            next;
        }

        warn "open($fname) success";

        warn "FILE_SKIP($fsize)";

        FILE_SKIP(\*F, $fsize);

        last;
    }

    my $pos = tell(F);
    warn "current position: $pos";

    while (1) {
        my $new_fname = "";

        my $count = 0;
        while (1) {
            my $line = <F>;

            if (! $line || $line eq '') {
                $count = ($count + 1) % 10;

                # warn "my_sleep(1)";
                my_sleep(1); # $count);

                if ($count == 0) {
                    warn "call SELECT_LAST()";
                    $new_fname = $this->SELECT_LAST();
                    if (defined $new_fname && $new_fname ne '') {
                        if ($new_fname eq $fname) {
                            warn "LAST FILE is same as prev one";
                        } else {
                            warn "NEXT FILE is created";
                            last;
                        }
                    }
                    next;
                }
                next;
            } else {
                $count = 0;
            }

            &{$this->{log_handle}}($line, $fname, $this->{log_name});
        }

        close(F);
        my_sleep(1);
        $fname = $new_fname;

        warn "open(F, $new_fname)";
        open(F, $new_fname);
    }
}

1;

__END__

# BEGIN { push @INC, "/usr/local/mobigen/CrediMail/modules"; };
# use LogTailLast

sub my_log_handler($$$)
{
    my ($line, $fname, $log_name) = @_;

    print $line;
}

TEST:
{
    my $p = new LogTailLast("/usr/local/mobigen/CrediMail/Data/LOG/QUEUE", "QUEUE");
    $p->{log_handle} = \&my_log_handler;
    $p->scan_log();
}

1;
__END__

