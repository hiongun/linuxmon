package LogTailRecent;

use strict qw(vars refs subs);
use Carp;

my $DEBUG = 0;
sub ASSERT($) { Carp::confess("ASSERT fail\n") if (! $_[0]); }

sub default_log_handle($$)
{
    my ($line, $fname) = @_;
    print "default_log_handle: [$fname] [$line]\n";
}

sub my_sleep($)
{
    my ($secs) = @_;

    select(undef, undef, undef, $secs);
}

sub LogTailRecent::new($$$)
{
    my ($classname, $log_file_pattern, $log_name) = @_;

    my $this = {};
       $this->{log_file_pattern} = $log_file_pattern;
       $this->{log_name} = $log_name;

       $this->{log_handle} = \&default_log_handle;

   bless($this, $classname);
}

sub FILE_SKIP($$)
{
    my ($fp, $fsize) = @_;

  # my $n_sum = 0;
  # while ($n_sum < $fsize) {
  #     my $line = <$fp>;
  #     if (! defined $line) {
  #         last;
  #     }
  #     $n_sum += length($line);
  # }

    seek($fp, $fsize, 0) or warn "$!";
}

sub LogTailRecent::SELECT_LAST($)
{
    my ($this) = @_;

    my $pattern = $this->{log_file_pattern};
    my @dirs = split(/\//, $pattern);
    my $filepattern  = pop @dirs;

    $filepattern =~ s/\*/\.\*/;

    my $dir = join("/", @dirs);

    warn "dir: $dir";
    warn "filepattern: $filepattern";

    my @files = ();
    local(*D);
    if (opendir(D, $dir)) {
        while (my $file = readdir(D)) {
            if ($file =~ /$filepattern/) {
                push @files, "$dir/$file";
            }
        }
    }
    closedir(D);

    my $last_mtime = 0;
    my $last_file = "";

    foreach my $file (@files) {
        my $mtime = (stat($file))[9];
        if ($last_mtime < $mtime) {
            $last_mtime = $mtime;
            $last_file = $file;
        }
    }

    warn "last_file: $last_file";

    return $last_file;
}

sub LogTailRecent::scan_log($)
{
    my ($this) = @_;

    my $fname = "";

    local(*F);

    while (1) {
        warn "call SELECT_LAST()";
        $fname = $this->SELECT_LAST();
        if (! $fname) {
            warn "this->SELECT_LAST() failed: $!";
            my_sleep(1);
            next;
        }

        warn "selected: $fname";

        my $fsize = -s $fname;
        if (! open(F, $fname)) {
            warn "open(F, $fname) failed: $!";
            my_sleep(1);
            next;
        }

        warn "open($fname) success";

        warn "FILE_SKIP($fsize)";

        FILE_SKIP(\*F, $fsize);

        last;
    }

    my $pos = tell(F);
    warn "current position: $pos";

    my $count = 0;
    while (1) {
        my $new_fname = "";

        my $rfd = '';
        while (1) {
            # warn "try read";
            my $line = <F>;

            if (! $line || $line eq '') {
                # warn "my_sleep(1)";
                my_sleep(1);
                $count = ($count+1) % 10;

                if ($count == 0) {
                    $new_fname = $this->SELECT_LAST();
                    if (defined $new_fname && $new_fname ne '') {
                        if ($new_fname eq $fname) {
                            warn "LAST FILE is same as prev one";
                        } else {
                            warn "NEXT FILE is created    $new_fname != $fname";
                            last;
                        }
                    }

                    next;
                }

                next;
            } else {
                $count = 0;
            }

            &{$this->{log_handle}}($line, $fname, $this->{log_name});
        }

        close(F);
        my_sleep(1);

        $fname = $new_fname;
        while (1) {
            if (! open(F, $fname)) {
                warn "open(F, $fname) failed: $!";
                my_sleep(1);
                next;
            }
            last;
        }
    }
}


1;

__END__

sub my_log_handler($$$)
{
    my ($line, $fname, $log_name) = @_;

    print $line;
}

TEST:
{
    my $p = new LogTailRecent("./lqms-*.log", "lqms");
    $p->{log_handle} = \&my_log_handler;
    $p->scan_log();
}

1;
__END__


