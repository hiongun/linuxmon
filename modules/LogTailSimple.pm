package LogTailSimple;

use strict qw(vars refs subs);
use Carp;

my $DEBUG = 0;
sub ASSERT($) { Carp::confess("ASSERT fail\n") if (! $_[0]); }

sub default_log_handle($$)
{
    my ($line, $fname) = @_;
    print "default_log_handle: [$fname] [$line]\n";
}

sub my_sleep($)
{
    my ($secs) = @_;

    select(undef, undef, undef, $secs);
}

sub LogTailSimple::new($$$)
{
    my ($classname, $log_file, $log_name) = @_;

    my $this = {};
       $this->{log_file} = $log_file;
       $this->{log_name} = $log_name;

       $this->{log_handle} = \&default_log_handle;

   bless($this, $classname);
}

sub FILE_SKIP($$)
{
    my ($fp, $fsize) = @_;

  # my $n_sum = 0;
  # while ($n_sum < $fsize) {
  #     my $line = <$fp>;
  #     if (! defined $line) {
  #         last;
  #     }
  #     $n_sum += length($line);
  # }

    seek($fp, $fsize, 0) or warn "$!";
}

sub LogTailSimple::scan_log($)
{
    my ($this) = @_;

    local(*F);

    my $fname = $this->{log_file};

    while (1) {
        my $fsize = -s $fname;
        if (! open(F, $fname)) {
            warn "open(F, $fname) failed: $!";
            my_sleep(1);
            next;
        }

        warn "open($fname) success";

        warn "FILE_SKIP($fsize)";

        FILE_SKIP(\*F, $fsize);

        last;
    }

    my $pos = tell(F);
    warn "current position: $pos";
    my $last_time = (stat($fname))[9];

    while (1) {

        while (1) {
            # warn "try read";
            my $line = <F>;

            if (! $line || $line eq '') {
                # warn "my_sleep(1)";

                my $mtime = (stat($fname))[9];
                my $new_fsize = -s $fname;
 
                if ($pos > $new_fsize || ($pos == $new_fsize && $mtime > $last_time)) {
                    warn "NEXT FILE is created.";
                    last;
                }

                my_sleep(1);
                next;
            }

            $last_time = time();

            &{$this->{log_handle}}($line, $fname, $this->{log_name});
            my_sleep(1);
        }

        close(F);
        my_sleep(1);

        while (1) {
            if (! open(F, $fname)) {
                warn "open(F, $fname) failed: $!";
                my_sleep(1);
                next;
            }
            $last_time = (stat($fname))[9];
            last;
            $pos = 0;
        }
    }
}


1;

__END__

sub my_log_handler($$$)
{
    my ($line, $fname, $log_name) = @_;

    print $line;
}

TEST:
{
    my $p = new LogTailSimple("./messages.log", "messages");
    $p->{log_handle} = \&my_log_handler;
    $p->scan_log();
}

1;
__END__


