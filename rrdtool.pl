#!/bin/env perl

BEGIN { push @INC, "./modules" }

use RRDEditor;

sub rrd_update($$$)
{
    my ($rrd_file, $key, $val) = @_;

    my $rrd = new RRDEditor();
    if (-e $rrd_file) {
        $rrd->open("$rrd_file");
    } else {
        $rrd->create("DS:$key:GAUGE:600:U:U RRA:AVERAGE:0.5:1:144000");
    }

    $rrd->update("N:$val");

    # $rrd->save($rrd_file);

    $rrd->close();
}

sub rrd_dump($$)
{
    my ($rrd_file, $key) = @_;

    my $rrd = new RRDEditor();
    if (! -e $rrd_file) {
        return;
    }

    $rrd->open($rrd_file);

    $rrd->dump();

    $rrd->close();
}

MAIN:
{
    if (! @ARGV) {
        print "Usage: $0 rrdfile.rrd UPDATE KEY VAL\n";
        print "       $0 rrdfile.rrd DUMP KEY\n";
        exit(0);
    }

    my $rrd_file = $ARGV[0];
    my $cmd = $ARGV[1];
    my $key = $ARGV[2];
    my $val = undef;
    if (@ARGV > 3) {
        $val = $ARGV[3];
    }

    if ($cmd eq 'UPDATE') {
        rrd_update($rrd_file, $key, $val);
    }

    if ($cmd eq 'DUMP') {
        rrd_dump($rrd_file, $key);
    }
}

