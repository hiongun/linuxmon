#!/bin/env perl

sub data_insert($$$)
{
    my ($key, $time, $val) = @_;

    $key =~ s/\//_/g;
    $key =~ s/\-/_/g;

    if (! -e "./data/$key.db") {
        my $cmd = "sqlite3 ./data/$key.db 'CREATE TABLE DATA_1M(time VARCHAR(16), val NUMBER); CREATE INDEX DATA_1M_IDX on DATA_1M(time)'";
        `$cmd`;
    }

    my $cmd = "sqlite3 ./data/$key.db 'INSERT INTO DATA_1M(time, val) VALUES ($time, $val)'";
    `$cmd`;
}

sub data_dump($)
{
    my ($key) = @_;
    my $cmd = "sqlite3 ./data/$key.db 'SELECT * FROM DATA_1M'";
    print `$cmd`;
}

data_insert("c", "10101010", 30);
data_insert("c", "10101011", 31);
data_insert("c", "10101012", 32);

data_dump("c");
