#!/bin/sh

ps -ef|grep linuxmon.pl|awk '{print $2}'| xargs kill -9 2> /dev/null
ps -ef|grep logtail.pl|awk '{print $2}'| xargs kill -9 2> /dev/null
