use YAMLTiny;

my $yaml = YAMLTiny->read('svc.conf');
$conf = $yaml->[0];
my $logs = $conf->{logs};
foreach my $log (@{$logs}) {
    warn "-----------\n";
    foreach my $key (keys %{$log}) {
        warn "log->{$key} = $log->{$key}\n";
    }
}

my $ports = $conf->{ports};

foreach my $port (@{$ports}) {
    warn "-----------\n";
    foreach my $key (keys %{$port}) {
        warn "port->{$key} = $port->{$key}\n";
    }
}
